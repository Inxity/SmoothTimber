package com.syntaxphoenix.spigot.smoothtimber.compatibility.blockylog;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;

import com.syntaxphoenix.blockylog.BlockyApi;
import com.syntaxphoenix.blockylog.data.BlockyStorage;
import com.syntaxphoenix.spigot.smoothtimber.utilities.PluginUtils;
import com.syntaxphoenix.spigot.smoothtimber.utilities.limit.IntCounter;
import com.syntaxphoenix.spigot.smoothtimber.utilities.locate.LocationResolver;
import com.syntaxphoenix.spigot.smoothtimber.utilities.locate.Locator;
import com.syntaxphoenix.spigot.smoothtimber.version.manager.VersionChanger;

public final class BlockyLogResolver_v2_x extends LocationResolver {

	private final BlockyApi api = BlockyApi.getApi();

	protected BlockyLogResolver_v2_x() {}

	@Override
	public void resolve(Location start, int radius, List<Location> current, IntCounter counter, int limit) {
		VersionChanger change = PluginUtils.CHANGER;
		World world = start.getWorld();
		int x = start.getBlockX();
		int y = start.getBlockY();
		int z = start.getBlockZ();

		BlockyStorage storage = api.getStorage(world);

		for (int cx = x - radius; cx <= x + radius; cx++) {
			for (int cz = z - radius; cz <= z + radius; cz++) {
				if (limit >= 0 && counter.get() >= limit) {
					return;
				}
				boolean checkLoc = true;
				if (cx == x && cz == z) {
					checkLoc = false;
				}
				Location location = new Location(world, cx, y, cz);
				if (change.isWoodBlock(Locator.getBlock(location))) {
					if (storage.getData(cx, y, cz).getPlayerId() != null || current.contains(location)) {
						continue;
					}
					current.add(location);
					counter.increment();
					if (checkLoc) {
						resolve(location, radius, current, counter, limit);
					}
				}
			}
		}
	}

	@Override
	public boolean isPlayerPlaced(Location location) {
		return api.isPlayerPlaced(location);
	}

}

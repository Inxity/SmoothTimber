package com.syntaxphoenix.spigot.smoothtimber.compatibility.jobsreborn;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;

import com.syntaxphoenix.spigot.smoothtimber.SmoothTimber;
import com.syntaxphoenix.spigot.smoothtimber.compatibility.CompatibilityAddon;
import com.syntaxphoenix.spigot.smoothtimber.config.STConfig;
import com.syntaxphoenix.spigot.smoothtimber.utilities.Container;
import com.syntaxphoenix.spigot.smoothtimber.utilities.plugin.PluginPackage;

public class JobsReborn extends CompatibilityAddon {

	private final Container<JobsRebornConfig> configContainer = Container.of();
	private final JobsRebornFallListener listener;

	public JobsReborn() {
		this.listener = new JobsRebornFallListener();
	}

	@Override
	public void onEnable(PluginPackage pluginPackage, SmoothTimber smoothTimber) {
		configContainer.replace(new JobsRebornConfig(this, pluginPackage));
		configContainer.ifPresent(STConfig::reload);
		Bukkit.getPluginManager().registerEvents(listener, smoothTimber);
	}

	@Override
	public void onDisable(SmoothTimber smoothTimber) {
		HandlerList.unregisterAll(listener);
		configContainer.replace(null);
	}

	@Override
	public JobsRebornConfig getConfig() {
		return configContainer.get();
	}

}

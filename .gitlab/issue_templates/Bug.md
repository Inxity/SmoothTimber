<!--- Provide a general summary of the issue in the Title above -->
/label ~Bug

### Specifications
<!--- e.g. Spigot 1.16.1 -->
Server: 
<!--- e.g. 1.8.3 -->
Version: 

Processor:
<!-- dedicated / virtual / unknown -->
- Type: 
- Specifications:
  <!-- e.g. Intel(R) Core(TM) i5-7600 -->
  - Name: 
  <!-- e.g. 3.50 GHz -->
  - Speed: 
  <!-- e.g. 4 -->
  - Cores: 
<!-- e.g. 1Gb -->
RAM: 

### Steps to Reproduce
<!--- Provide set of steps to reproduce this bug. -->
1.
2.
3.
4.

### Detailed Description
<!--- Provide a detailed description of what happend -->
<!--- Please don't forget to provide logs for your issue -->